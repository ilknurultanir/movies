import Layout from '../components/Layout'
import Results from '../components/Results'




class Index extends React.Component{
    
    constructor(props){
        super(props);
        this.state={
            search:undefined,
            movie:undefined
        }
        this.searchMovie = async (search)=>{
            if(search.length>0){
                try {
                    const res = await fetch(`https://www.omdbapi.com/?t=${search}&plot=full&apikey=b5aa8dc9`);
                    const data = await res.json();
                    console.log(data)
                    this.setState({movie:data})
                }
                catch (error) {
                    console.log(error);
                }
            }else{
                this.setState({
                    movie:undefined
                })
            }
        }
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e){
        this.setState({
            search:e.target.value
        })
    }

    render(){
    let movieData;

    if (this.state.movie !== undefined && this.state.movie) {
        movieData = <div className="row">
            <div className="col-sm-6 d-flex justify-content-center align-items-center">
                <img src={this.state.movie.Poster} className="img-fluid"/>
            </div>
            <div className="col-sm-6 pt-5 pb-5">
                <h1>{this.state.movie.Title}</h1>
                <p className="lead">{this.state.movie.Plot}  </p>
                <p className="badge badge-primary">{this.state.movie.Rated}</p> 
            </div> 
        </div>;
    }else if(this.state.movie !== undefined && this.state.movie && this.state.movie.Error){
        movieData =<div className="row">
        <div className="col-12 d-flex justify-content-center align-items-center">
            <h1>We couldn't find such a film</h1>
        </div>
    </div>;
    } else {
        movieData =<div className="row">
        <div className="col-12 d-flex justify-content-center align-items-center">
            <h1>Think of a word and find a movie to watch, lol</h1>
        </div>
    </div>;
    }
        return(
        <Layout>
            <form>
                <div className="form-group row">
                    <div className="col-sm-12 d-flex">
                        <input type="text" className="form-control flex-fill" name="inputName" id="inputName" placeholder="Search a movie" value={this.state.search} onChange={e => this.searchMovie(e.target.value)}  />
                    </div>
                </div>
            </form>
            <div className="container pt-5 pb-5">
                {movieData}
            </div>
        </Layout>
        )
    }
}

export default Index;