import Link from 'next/link';

class Navbar extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          menuVisible: "collapse"
        };
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        if(this.state.menuVisible=="collapse"){
            this.setState({menuVisible: "collapsed"});
        }else{
            this.setState({menuVisible: "collapse"});
        } 
    }
    render(){
        return(
        <div>
            <nav className="navbar navbar-expand-sm navbar-light bg-light">
            <div className="container">
                
           
                <Link href="/">
                    <a className="navbar-brand">Movies</a>
                </Link>
                <button className="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#col" aria-controls="col"
                    aria-expanded="false" aria-label="Toggle navigation"  onClick={this.handleClick}>
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className={"navbar-collapse"+" "+this.state.menuVisible} id="col">
                    <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li className="nav-item active">
                            <Link href="https://bitbucket.org/ilknurultanir/movies"><a target="_blank" className="nav-link">Repo</a></Link>
                        </li>
                    </ul>
                </div>
                </div>
            </nav>
        </div>
        )
    }
}

export default Navbar;